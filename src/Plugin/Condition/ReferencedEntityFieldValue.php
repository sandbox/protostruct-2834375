<?php

namespace Drupal\referenced_entity_field_condition\Plugin\Condition;

use Drupal\Component\Utility\Html;
use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Condition plugin to match a field value of a referenced entity.
 *
 * @Condition(
 *   id = "referenced_entity_field",
 *   label = @Translation("Referenced entity field value"),
 *   context = {
 *     "node" = @ContextDefinition("entity:node",
 *       label = @Translation("node")
 *     )
 *   }
 * )
 */
class ReferencedEntityFieldValue extends ConditionPluginBase implements ContainerFactoryPluginInterface  {

  /**
   * Operators.
   */
  const OPERATOR_EQUAL = '=';
  const OPERATOR_NOT_EQUAL = '!=';
  const OPERATOR_IN = 'IN';
  const OPERATOR_NOT_IN = 'NOT IN';
  const OPERATOR_GREATER_THAN = '>';
  const OPERATOR_GREATER_EQUAL = '>=';
  const OPERATOR_LESS_THAN = '<';
  const OPERATOR_LESS_EQUAL = '<=';
  const OPERATOR_REGEX = 'REGEX';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityBundleInfo;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * 2D Array of all entity field definitions keyed by entity type ID first,
   * then by field name.
   *
   * @var \Drupal\Core\Field\FieldDefinitionInterface[]
   */
  protected $fieldInfo;

  /**
   * Creates a new TermValue instance.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_bundle_info
   *   The entity type bundle info service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              EntityTypeManagerInterface $entity_type_manager,
                              EntityTypeBundleInfoInterface $entity_bundle_info,
                              EntityFieldManagerInterface $entity_field_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->entityBundleInfo = $entity_bundle_info;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [] + parent::defaultConfiguration();
  }

  /**
   * Get the default configuration but don't store it like static::defaultConfiguration() causes to happen.
   *
   * @return array
   *   The default configuration.
   */
  protected function defaultConfigurationValues() {
    return [
      'reference_field' => NULL,
      'field' => NULL,
      'operator' => static::OPERATOR_EQUAL,
      'value' => NULL,
      'separator' => ',',
    ] + parent::defaultConfiguration();
  }

  /**
   * Retrieve a configuration value defaulting to value from static::defaultConfigurationValues().
   *
   * @param $key
   *   The configuration key.
   *
   * @return mixed
   *   The configuration value or NULL if the key does not exist.
   */
  protected function getConfigurationValue($key) {
    if (isset($this->configuration[$key])) {
      return $this->configuration[$key];
    }
    else {
      $defaults = $this->defaultConfigurationValues();
      if (isset($defaults[$key])) {
        return $defaults[$key];
      }
      else {
        return NULL;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // Load configuration.
    $reference_field = $this->getConfigurationValue('reference_field');
    $field = $this->getConfigurationValue('field');
    $operator = $this->getConfigurationValue('operator');
    $value = $this->getConfigurationValue('value');
    $separator = $this->getConfigurationValue('separator');

    $form['reference_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Content reference field'),
      '#default_value' => $reference_field,
      '#description' => $this->t('Optionally select a reference field on the context node. Select <em>- All -</em> to search all reference fields.'),
      '#options' => [NULL => $this->t('- All -')],
    ];
    if (!empty($node_fields = $this->getFieldInfo()['node'])) {
      /** @var \Drupal\Core\Field\FieldDefinitionInterface $field_definition */
      foreach ($node_fields as $field_name => $field_definition) {
        if ($field_definition->getType() == 'entity_reference') {
          $field_string = "{$field_definition->getLabel()} ($field_name)";
          $form['reference_field']['#options'][$field_name] = $field_string;
        }
      }
    }

    $form['field'] = [
      '#type' => 'select',
      '#title' => $this->t('Referenced entity field'),
      '#default_value' => $field,
      '#options' => [NULL => $this->t('- Please select a field -')],
      '#description' => $this->t('Select the field on the referenced entity to match against.'),
    ];
    /** @var \Drupal\Core\Field\FieldDefinitionInterface[] $fields */
    foreach ($this->getFieldInfo() as $entity_type_id => $fields) {
      $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
      $entity_type_label = (string) $entity_type->getLabel();
      foreach ($fields as $field_name => $field_definition) {
        $field_string = "{$field_definition->getLabel()} ($field_name)";
        $form['field']['#options'][$entity_type_label]["{$entity_type_id}:{$field_name}"] = $field_string;
      }
    }

    $form['operator'] = [
      '#type' => 'select',
      '#title' => $this->t('Operator'),
      '#description' => $this->t('Select the comparision operator.'),
      '#default_value' => $operator,
      '#options' => static::getOperatorOptions(),
      '#id' => Html::getUniqueId('reference-entity-field-condition-operator'),
    ];

    $form['value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Value'),
      '#default_value' => $value,
      '#description' =>
        $this->t('Value to match against. Special formats:') . '<br/>' .
        $this->t('@op: A list of values separated by %sep as specified.', [
          '@op' => static::OPERATOR_IN . ', ' . static::OPERATOR_NOT_IN,
          '%sep' => $this->t('Separator')
        ]) . '<br/>' .
        $this->t('@op: An enclosed regular expression.', [
          '@op' => static::OPERATOR_REGEX
        ]),
    ];

    $form['separator'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Separator'),
      '#default_value' => $separator,
      '#states' => [
        'visible' => [
          ['select#' . $form['operator']['#id'] => ['value' => static::OPERATOR_IN]],
          ['select#' . $form['operator']['#id'] => ['value' => static::OPERATOR_NOT_IN]],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['reference_field'] = $form_state->getValue('reference_field');
    $this->configuration['field'] = $form_state->getValue('field');
    $this->configuration['operator'] = $form_state->getValue('operator');
    $this->configuration['value'] = $form_state->getValue('value');
    $this->configuration['separator'] = $form_state->getValue('separator');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    if (empty($this->configuration['field'])) {
      return TRUE;
    }

    // Load configuration.
    $reference_field = $this->getConfigurationValue('reference_field');
    $field = $this->getConfigurationValue('field');
    $operator = $this->getConfigurationValue('operator');
    $value = $this->getConfigurationValue('value');
    $separator = $this->getConfigurationValue('separator');

    /** @var \Drupal\node\NodeInterface $node */
    $node = $this->getContextValue('node');

    $field_parts = explode(':', $field);
    $entity_type_id = reset($field_parts);
    $field_name = next($field_parts);

    // Fetch referenced entities either through configured reference field or
    // using \Drupal\Core\Entity\EntityInterface::referencedEntities().
    /** @var \Drupal\Core\Entity\EntityInterface[] $referenced_entities */
    $referenced_entities = [];
    if (!empty($reference_field)) {
      if ($node->hasField($reference_field)) {
        $referenced_entities = $node->get($reference_field)->referencedEntities();
      }
    }
    else {
      $referenced_entities = $node->referencedEntities();
    }

    foreach ($referenced_entities as $referenced_entity) {
      if ($referenced_entity instanceof FieldableEntityInterface && $referenced_entity->getEntityTypeId() == $entity_type_id) {
        if ($referenced_entity->hasField($field_name)) {
          $field_value = $this->massageFieldValue($referenced_entity->get($field_name));
          if ($this->matchFieldValue($field_value, $operator, $value, $separator)) {
            return TRUE;
          }
        }
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {

    // Load configuration.
    $reference_field = $this->getConfigurationValue('reference_field');
    $field = $this->getConfigurationValue('field');
    $operator = $this->getConfigurationValue('operator');
    $value = $this->getConfigurationValue('value');
    $separator = $this->getConfigurationValue('separator');

    if (in_array($operator, [static::OPERATOR_IN, static::OPERATOR_NOT_IN])) {
      $value = explode($separator, $value);
      $value = array_map(function ($x) { return trim($x); }, $value);
      $value = '[' . implode($separator, $value) . ']';
    }

    $params = [
      '@operator' => $operator,
      '%value' => $value,
      '%through' => !empty($reference_field) ? $reference_field : 'any reference field',
    ];

    $field_info = $this->getFieldInfo();
    $field_parts = explode(':', $field);
    $entity_type_id = reset($field_parts);
    $field_name = next($field_parts);
    $field_definition = isset($field_info[$entity_type_id][$field_name]) ? $field_info[$entity_type_id][$field_name] : NULL;

    if ($field_definition instanceof FieldDefinitionInterface) {
      $params['%field'] = $field;
    }
    else {
      $params['@field'] = "!unknown:$field";
      $params['@warning'] = $this->t('WARNING: This condition will never match.');
    }

    return $this->t('Match field %field @operator %value on entity referenced through %through. @warning', $params + ['@warning' => '']);
  }

  /**
   * Retrieve field information.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   2D array of field definitions, keyed by entity type ID first, then by
   *   field name.
   */
  protected function getFieldInfo() {
    if (empty($this->fieldInfo)) {
      $this->fieldInfo = [];
      foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
        if ($this->isSupportedEntityType($entity_type)) {
          foreach ($this->entityBundleInfo->getBundleInfo($entity_type_id) as $bundle => $bundle_info) {
            foreach ($this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle) as $field_name => $field_definition) {
              if ($this->isSupportedField($field_definition) && !isset($this->fieldInfo[$entity_type_id][$field_name])) {
                $this->fieldInfo[$entity_type_id][$field_name] = $field_definition;
              }
            }
          }
        }
      }
    }
    return $this->fieldInfo;
  }

  /**
   * Check if an entity type is supported.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return bool
   *   TRUE is the entity type is supported, FALSE otherwise.
   */
  protected function isSupportedEntityType(EntityTypeInterface $entity_type) {
    return $entity_type instanceof ContentEntityTypeInterface;
  }

  /**
   * Check if a field is supported.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   *
   * @return bool
   *   TRUE is the field is supported, FALSE otherwise.
   */
  protected function isSupportedField(FieldDefinitionInterface $field_definition) {
    return TRUE;
  }

  /**
   * Massage a field item list for matching.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $field_item_list
   *   The field item list.
   *
   * @return string
   *   A string representing the item list value.
   */
  protected function massageFieldValue(FieldItemListInterface $field_item_list) {
    switch ($field_item_list->getFieldDefinition()->getType()) {
      case 'entity_reference':
        return $field_item_list->value[0]['target_id'];

      default:
        return $field_item_list->value;
    }
  }

  /**
   * Match a field value against a give value using an operator.
   *
   * @param mixed $field_value
   *   The field value.
   * @param string $operator
   *   The operator.
   * @param mixed $value
   *   The value to test against.
   * @param string $separator
   *   Separator for IN operator.
   *
   * @return bool
   *   TRUE is the field values matches the given value according to the
   *   operator.
   */
  protected static function matchFieldValue($field_value, $operator, $value, $separator) {
    switch ($operator) {
      default:
      case static::OPERATOR_EQUAL:
        return $field_value == $value;

      case static::OPERATOR_NOT_EQUAL:
        return $field_value != $value;

      case static::OPERATOR_IN:
        return in_array($field_value, explode($separator, $value));

      case static::OPERATOR_NOT_IN:
        return !in_array($field_value, explode($separator, $value));

      case static::OPERATOR_GREATER_THAN:
        return $field_value > $value;

      case static::OPERATOR_GREATER_EQUAL:
        return $field_value >= $value;

      case static::OPERATOR_LESS_THAN:
        return $field_value < $value;

      case static::OPERATOR_LESS_EQUAL:
        return $field_value <= $value;

      case static::OPERATOR_REGEX:
        return preg_match($value, $field_value);
    }
  }

  /**
   * Retrieve array with operator options.
   *
   * @return array
   *   Array of operator labels keyed by operator ID.
   */
  protected static function getOperatorOptions() {
    return [
      static::OPERATOR_EQUAL => static::OPERATOR_EQUAL,
      static::OPERATOR_NOT_EQUAL => static::OPERATOR_NOT_EQUAL,
      static::OPERATOR_IN => static::OPERATOR_IN,
      static::OPERATOR_NOT_IN => static::OPERATOR_NOT_IN,
      static::OPERATOR_GREATER_THAN => static::OPERATOR_GREATER_THAN,
      static::OPERATOR_GREATER_EQUAL => static::OPERATOR_GREATER_EQUAL,
      static::OPERATOR_LESS_THAN => static::OPERATOR_LESS_THAN,
      static::OPERATOR_LESS_EQUAL => static::OPERATOR_LESS_EQUAL,
      static::OPERATOR_REGEX => static::OPERATOR_REGEX,
    ];
  }

}
